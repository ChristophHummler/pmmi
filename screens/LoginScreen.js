import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, Button, TouchableWithoutFeedback } from 'react-native';
import { RectButton, ScrollView } from 'react-native-gesture-handler';
import Layout from '../constants/Layout';

export default function LoginScreen(props) {
    return (
        <View style={styles.container}>
            <ImageBackground style={styles.imageBackground} source={require('../assets/images/town.jpg')}>
                <View style={styles.logo} />
                <View style={styles.buttonContainer}><Text>Registrieren</Text></View>
                <TouchableWithoutFeedback onPress={() => props.navigation.navigate('Root')}><View style={styles.buttonContainer}><Text>Einloggen</Text></View></TouchableWithoutFeedback>
            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fafafa',
    },
    imageBackground: {
        width: Layout.window.width,
        flex: 1,
        alignItems: 'center',
        paddingTop: 100,
        padding: 40
    },
    logo: {
        height: 200,
        width: 200,
        backgroundColor: 'white',
        borderRadius: 100,
        marginBottom: 30
    },
    buttonContainer: {
        backgroundColor: 'white',
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        marginBottom: 20
    }
});
