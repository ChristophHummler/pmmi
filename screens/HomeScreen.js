import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View, TextInput } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Layout from '../constants/Layout';

import { MonoText } from '../components/StyledText';
import { MaterialIcons } from '@expo/vector-icons';

export default function HomeScreen() {
  return (
    <View style={styles.container}>
      <TextInput style={styles.textInput} placeholder='Zum suchen tippen...' />
      <View style={styles.categoriesContainer}>
        <View style={styles.categoryContainer}><Text>Bücher</Text></View>
        <View style={styles.categoryContainer}><Text>Schreibwaren</Text></View>
        <View style={styles.categoryContainer}><Text>Zubehör</Text></View>
      </View>
      <View style={styles.bottomContainer}>
        <View style={styles.puller} />
        <Text style={styles.titleText}>Browse</Text>
        <View style={styles.itemsContainer}>
          <View style={styles.itemContainer}>
            <Image style={styles.image} source={{ uri: 'https://www.thebestsocial.media/de/wp-content/uploads/sites/3/2019/09/kimberly-farmer-lUaaKCUANVI-unsplash.jpg' }} />
            <Text style={styles.subtitleText}>UX For Beginners</Text>
            <View style={styles.starsContainer}>
              <MaterialIcons name='star' size={20} />
              <MaterialIcons name='star' size={20} />
              <MaterialIcons name='star' size={20} />
            </View>
            <Text style={styles.priceText}>8,50€</Text>
          </View>
          <View style={styles.itemContainer}>
            <Image style={styles.image} source={{ uri: 'https://www.thebestsocial.media/de/wp-content/uploads/sites/3/2019/09/kimberly-farmer-lUaaKCUANVI-unsplash.jpg' }} />
            <Text style={styles.subtitleText}>Statistik 2</Text>
            <View style={styles.starsContainer}>
              <MaterialIcons name='star' size={20} />
              <MaterialIcons name='star' size={20} />
              <MaterialIcons name='star-border' size={20} />
            </View>
            <Text style={styles.priceText}>9,50€</Text>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2D3E50',
    padding: 20
  },
  textInput: {
    backgroundColor: 'white',
    height: 50,
    borderRadius: 50,
    paddingHorizontal: 20,
    marginBottom: 20
  },
  categoriesContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  categoryContainer: {
    backgroundColor: 'white',
    padding: 5,
    borderRadius: 10,
    width: (Layout.window.width - 40) / 3 - 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  bottomContainer: {
    backgroundColor: 'white',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    position: 'absolute',
    bottom: 0,
    width: Layout.window.width,
    height: Layout.window.height - 400,
    marginTop: 'auto',
    padding: 20
  },
  puller: {
    width: 40,
    height: 5,
    backgroundColor: 'lightgray',
    borderRadius: 50,
    alignSelf: 'center',
    marginBottom: 20
  },
  titleText: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 20
  },
  itemsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  itemContainer: {
    alignItems: 'center'
  },
  image: {
    width: (Layout.window.width - 40) / 2 - 20,
    height: 220,
    borderRadius: 30,
    backgroundColor: 'red',
    marginBottom: 5
  },
  subtitleText: {
    fontWeight: 'bold'
  },
  starsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  priceText: {
    fontSize: 12
  }
});
